﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void MassiveIn(ref int[,] numbers)
        {
            Random rand = new Random();
            for(int i = 0; i<=numbers.GetLength(0)-1; i++)
            {
                for(int j = 0; j <= numbers.GetLength(1)-1  ; j++)
                {
                   numbers[i, j] = rand.Next(-10,10);
                }
            }
        }
        static void MassiveOut(int [,] numbers)
        {
            for (int i = 0; i <= numbers.GetLength(0)-1; i++)
            {
                for (int j = 0; j <= numbers.GetLength(1)-1; j++)
                {
                    Console.Write($"{numbers[i, j]} ");
                }
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Task");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task)
            {
                case 1:
                    Console.WriteLine("Enter A and B");
                    int a = Convert.ToInt32(Console.ReadLine());
                    int b = Convert.ToInt32(Console.ReadLine());
                    int[,] numbers = new int[a, b];
                    MassiveIn(ref numbers);
                    MassiveOut(numbers);
                    Console.WriteLine("Enter n and m");
                    int n = Convert.ToInt32(Console.ReadLine());
                    int m = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (i == n - 1)
                            {
                                Console.Write($"{numbers[i, j]} ");
                            }
                        }
                    }
                    Console.WriteLine();
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (j == m - 1)
                            {
                                Console.Write($"{numbers[i, j]} ");
                            }
                        }
                    }
                    Console.WriteLine();
                    int summ3 = 0;
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (i == 2)
                            {
                                summ3 = summ3 + numbers[i, j];
                            }
                        }
                    }
                    Console.WriteLine($"Summa 3 stroki = {summ3}");
                    break;
                case 2:
                    Console.WriteLine("Enter A and B");
                    a = Convert.ToInt32(Console.ReadLine());
                    b = Convert.ToInt32(Console.ReadLine());
                    numbers = new int[a, b];
                    MassiveIn(ref numbers);
                    MassiveOut(numbers);
                    Console.WriteLine("Switch massive:");
                    for(int i = numbers.GetLength(0) - 1; i>= 0; i--)
                    {
                        for(int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            Console.Write($"{numbers[i, j]} ");
                        }
                        Console.WriteLine();
                    }
                    break;
                case 3:
                    numbers = new int[18, 36];
                    Random rand = new Random();
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            numbers[i, j] = rand.Next(0,2);
                        }
                    }
                    MassiveOut(numbers);
                    int svmest = 0;
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (numbers[i, j] == 0)
                            {
                                ++svmest;
                            }
                        }
                    }
                    Console.WriteLine($"Number of free places = {svmest}");
                    break;
                case 4:
                    Console.WriteLine("Enter A and B");
                    a = Convert.ToInt32(Console.ReadLine());
                    b = Convert.ToInt32(Console.ReadLine());
                    numbers = new int[a, b];
                    MassiveIn(ref numbers);
                    MassiveOut(numbers);
                    int sumstr = 0;
                    int sumstl = 0;
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        int minstr = 11;
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (numbers[i, j] < minstr)
                            {
                                minstr = numbers[i,j];
                            }
                            if(j == numbers.GetLength(1)-1)
                            {
                                sumstr = sumstr + minstr;
                            }
                        }
                    }
                    for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                    {
                        int maxstl = -11;
                        for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                        {
                            if (numbers[i, j] > maxstl)
                            {
                                maxstl = numbers[i, j];
                            }
                            if (i == numbers.GetLength(0)-1)
                            {
                                sumstl = sumstl + maxstl;
                            }
                        }
                    }
                    Console.WriteLine($"Min summ strok = {sumstr}");
                    Console.WriteLine($"Max sum stolbcov = {sumstl}");
                    break;
                case 5:
                    Console.WriteLine("Enter A");
                    a = Convert.ToInt32(Console.ReadLine());
                    numbers = new int[a, a];
                    MassiveIn(ref numbers);
                    MassiveOut(numbers);
                    int summdiogonal = 0;
                    int countpdche = 0;
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (i == j)
                            {
                                summdiogonal = summdiogonal + numbers[i,j];
                            }
                            if  (i == a-1-j && (numbers[i,j]%2 == 0))
                            {
                                countpdche++;
                            }
                        }
                    }
                    Console.WriteLine($"Summa dioganali = {summdiogonal}");
                    Console.WriteLine($"Kol-vo chetnih elementov poboxhnoy diogonali = {countpdche}");
                    break;
                case 6:
                    Console.WriteLine("Enter A and B");
                    a = Convert.ToInt32(Console.ReadLine());
                    b = Convert.ToInt32(Console.ReadLine());
                    numbers = new int[a, b];
                    MassiveIn(ref numbers);
                    MassiveOut(numbers);
                    int []summchpe = new int[b];
                    int[] maxonmod = new int[a];
                    for (int i = 0; i <= b-1; i++)
                    {
                        summchpe[i] = 0;
                    }
                    for (int i = 0; i <= a-1; i++)
                    {
                        maxonmod[i] = 0;
                    }
                    for (int j = 0; j<= numbers.GetLength(1)-1; j++)
                    {
                        for (int i = 0; i<= numbers.GetLength(0)-1; i++)
                        {
                            if(numbers[i,j]>0 && numbers[i,j]%2 == 0)
                            {
                                summchpe[j] = summchpe[j] + numbers[i, j];
                            }
                        }
                    }
                    for (int i = 0; i <= numbers.GetLength(0) - 1; i++)
                    {
                        for (int j = 0; j <= numbers.GetLength(1) - 1; j++)
                        {
                            if (Math.Abs(numbers[i, j]) > Math.Abs(maxonmod[i]))
                            {
                                maxonmod[i] = numbers[i, j];
                            }
                        }
                    }
                    Console.WriteLine("Massive chetnih polozh elementov:");
                    for(int i = 0; i <= b-1; i++)
                    {
                        Console.Write($"{summchpe[i]} ");
                    }
                    Console.WriteLine();
                    Console.WriteLine("massive naibolshih po modulyu elementov:");
                    for (int i = 0; i <= a-1; i++)
                    {
                        Console.Write($"{maxonmod[i]} ");
                    } 
                    break;
                default:
                    Console.WriteLine("Task doesn't exist");
                    break;

            }
        }
    }
}
