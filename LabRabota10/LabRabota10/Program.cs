﻿using System;
using System.Text.RegularExpressions;

namespace LabRabota10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of task");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task)
            {
                case 1:
                    Console.WriteLine("Write anything");
                    string str = Console.ReadLine();
                    Console.WriteLine("Write number N");
                    int wlength = Convert.ToInt32(Console.ReadLine());
                    foreach (Match match in Regex.Matches(str, @"\b\w+\b"))
                    {
                        if (match.Length <= wlength) Console.WriteLine(match);
                    }
                    break;
                case 2:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    foreach(Match match in Regex.Matches(str, @"\b\w+\b"))
                    {
                        if (char.IsLower(Convert.ToString(match)[0])) Console.WriteLine(match);
                    }
                    break;
                case 3:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string strv = str;
                    foreach (Match match1 in Regex.Matches(str, @"\b\w+\b"))
                    {
                        foreach(Match match2 in Regex.Matches(str, @"\b\w+\b"))
                            {
                            if((match1.Value == match2.Value) && (match1.Index != match2.Index))
                            {
                                string pattern = Convert.ToString(match1);
                                strv = Regex.Replace(strv, pattern, "");

                            }
                        }
                    }
                    Console.WriteLine(strv);
                    break;
                case 4:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    int count = 0;
                    Console.WriteLine("Write word");
                    string word = Console.ReadLine();
                    foreach (Match match in Regex.Matches(str, @"\b" + $"{word}" + @"\b")) ++count;
                    Console.WriteLine(count);
                    break;
                case 5:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string max = "w";
                    foreach(Match match in Regex.Matches(str, @"\b\w+\b"))
                    {
                        if (Convert.ToString(match).Length >= max.Length) max = Convert.ToString(match);
                    }
                    Console.WriteLine($"Max long word - {max}");
                    break;
                case 6:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    foreach (Match match1 in Regex.Matches(str, @"\b\w+\b"))
                    {
                        bool pvtr = false;
                        foreach (Match match2 in Regex.Matches(str, @"\b\w+\b"))
                        {
                            if ((match1.Value == match2.Value) && (match1.Index != match2.Index))
                            {
                                pvtr = true;
                            }
                        }
                        if (!pvtr)
                        {
                            Console.WriteLine(match1);
                        }
                    }
                    break;
                case 7:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    Console.WriteLine("Write number");
                    int n = Convert.ToInt32(Console.ReadLine());
                    strv = str;
                    foreach (Match match1 in Regex.Matches(str, @"\b\w+\b"))
                    {
                        count = 1;
                        foreach (Match match2 in Regex.Matches(str, @"\b\w+\b"))
                        {
                            if ((match1.Value == match2.Value) && (match1.Index != match2.Index)) ++count;
                        }
                        if (count > n) Console.WriteLine(match1);
                    }
                    break; 
                case 8:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string[] ddwq = Regex.Split(str, @"\b+");
                    for (int i = 1; i < (ddwq.Length - 1); i=i+2)
                    {
                        for (int j = (i + 2); j < ddwq.Length; j=j+2)
                        {
                            if (char.ToUpper((Convert.ToString(ddwq[i]))[0])> char.ToUpper((Convert.ToString(ddwq[j]))[0]))
                            {
                                string vremen = ddwq[i];
                                ddwq[i] = ddwq[j];
                                ddwq[j] = vremen;
                            }
                        }
                    }
                    for (int i = 1; i <= ddwq.Length-1; i=i+2) Console.WriteLine(ddwq[i]);
                    break;
                case 9:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string[] words = Regex.Split(str, @"\b+");
                    for (int i = 1; i < (words.Length - 1); i = i + 2)
                    {
                        for (int j = (i + 2); j < words.Length; j = j + 2)
                        {
                            if (words[i].Length>words[j].Length)
                            {
                                string vremen = words[i];
                                words[i] = words[j];
                                words[j] = vremen;
                            }
                        }
                    }
                    for (int i = 1; i <= words.Length - 1; i = i + 2) Console.WriteLine(words[i]);
                    break;
            }
            }
    }
}
