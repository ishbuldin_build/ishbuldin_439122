﻿using System;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=============FIRST=============");
            Random rand = new Random();
            int[] Weights = new int[20];
            for (int i = 0; i < 20 ; i++)
            {
                int x = rand.Next(50, 101);
                Weights[i] = x;
                Console.WriteLine($"{i} = {Weights[i]}");
            }
            Console.WriteLine("=============SECOND=============");
            int[] Array = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Array[i] = rand.Next(0, 100);
                Console.WriteLine($"{i} = {Array[i]}");
            }
            for (int i = 0; i < 10; i++)
            {
                Array[i] = Array[i]*2;
                Console.WriteLine($"{i} = {Array[i]}");
            }
            int firstarray = Array[0];
            for (int i = 0; i < 10; i++)
            {
                Array[i] = Array[i] / firstarray;
                Console.WriteLine($"{i} = {Array[i]}");
            }
            Console.WriteLine("Enter A");
            int a = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < 10; i++)
            {
                Array[i] = Array[i] - a;
                Console.WriteLine($"{i} = {Weights[i]}");
            }
            int[] Array2 = new int[10];
            Console.WriteLine("=============THIRD=============");
            for (int i = 0; i < 10; i++)
            {
                Array2[i] = rand.Next(0, 100);
                Console.WriteLine($"{i} = {Array2[i]}");
            }
            int summarray2 = 0;
            for (int i = 0; i < 10; i++)
            {
                summarray2 = summarray2 + Array2[i];
            }
            if (summarray2%2 == 0)
            {
                Console.WriteLine("Chetnoe");
            } else
            {
                Console.WriteLine("Nechetnoe");
            }
            summarray2 = 0;
            for (int i = 0; i < 10; i++)
            {
                summarray2 = summarray2 + Array2[i]*Array2[i];
            }
            if (summarray2 /10000 >= 1)
            {
                Console.WriteLine("5-znach");
            }
            else
            {
                Console.WriteLine("Ne 5-znach");
            }

            Console.WriteLine("=============FOURTH=============");
            int sumchet = 0;
            int sumnechet = 0;
            int[] houses = new int[10];
            for (int i = 0; i < 10; i++)
            {
                houses[i] = rand.Next(0, 11);
                Console.WriteLine($"{i} = {houses[i]}");
                if (i % 2 == 0)
                {
                    sumchet = sumchet + houses[i];
                }
                else
                {
                    sumnechet = sumnechet + houses[i];
                }
            }
            if (sumnechet > sumchet)
            {
                Console.WriteLine("Nechet bolshe");
            } else
            {
                Console.WriteLine("Chet bolshe");
            }
            Console.WriteLine("=============FIFTH=============");
            int izm = 0;
            int[] LastArray = new int[10];
            for (int i = 0; i < 10; i++)
            {
                LastArray[i] = rand.Next(-10, 10);
                Console.WriteLine($"{i} = {LastArray[i]}");
            }
            for (int i = 1; i < 10; i++)
            {
                if ((LastArray[i] < 0 && LastArray[i-1] >= 0)|| (LastArray[i] >= 0 && LastArray[i - 1] < 0))
                {
                    izm++;
                }
            }
            Console.WriteLine($"Izm = {izm}");
        }
    }
}
