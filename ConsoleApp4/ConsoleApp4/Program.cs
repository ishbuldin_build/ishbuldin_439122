﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static int Min( int a, int b)
        {
            if (a < b)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
        static int Func1( int d)
        {
            return Convert.ToInt32(Math.Pow(d, 3) - Math.Sin(d * Math.PI / 180));
        }
        static int Func2(int x)
        {
            return x % 100 / 10;
        }
        static double Func3(double n)
        {
            return Math.Pow(n, 0.5) + n;
        }
        static int Func5(int a)
        {
            if (a%5 == 0)
            {
                return a / 5;
            }
            else
            {
                return a + 1;
            }
        }
        static int Func4(int x)
        {
            if (x % 2 == 0)
            {
                return x / 2;
            }
            else
            {
                return 0;
            }
        }
        static double FuncOne(double n)
        {
            if (n>= 0.9)
            {
                return (1 / Math.Pow((0.1 + n), 2));
            } else if ((n < 0.9) && (n >= 0))
            {
                return (0.2*n + 0.1);
            } else
            {
                return (Math.Pow(n, 2)+ 0.2);
            }
        }
        static double FuncTwo  (double n)
        {
            if (Math.Abs(n) < 3)
            {
                return Math.Sin(n*Math.PI/180);
            }
            else if ((Math.Abs(n) < 9) && (Math.Abs(n) >= 3))
            {
                return Math.Pow(Math.Pow(n,2)+1,0.5)/ Math.Pow(Math.Pow(n, 2) + 5, 0.5);
            }
            else
            {
                return Math.Pow(Math.Pow(n, 2) + 1, 0.5) - Math.Pow(Math.Pow(n, 2) + 5, 0.5);
            }
        }
        static double FuncThree(double n, double a)
        {
            if (n < a)
            {
                return 0;
            }
            else if (n > a)
            {
                return (n - a)/(n+a);
            }
            else
            {
                return 1;
            }
        }
        static double FuncFour(double n)
        {
            if (Math.Abs(n) <= 0.1)
            {
                return Math.Pow(n,3) - 0.1;
            }
            else if ((Math.Abs(n)<=0.2) && (Math.Abs(n) > 0.1))
            {
                return 0.2*n - 0.1;
            }
            else
            {
                return Math.Pow(n,3) + 0.1;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter X and Y");
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            int z = Min(3 * x, 2 * y) + Min(x - y, x + y);
            Console.WriteLine($"z = {z}");
            Console.WriteLine("Enter a and b");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            if (Func1(a) == Min((Func1(a)), (Func1(b)))) 
            {
                Console.WriteLine("Min in A");
            } else
            {
                Console.WriteLine("Min in B");
            }
            Console.WriteLine("Enter a,b and c");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            z = Func2(a) + Func2(b) - Func2(c);
            Console.WriteLine($"Z = {z}");
            Console.WriteLine($"Otvet = {(Func3(6)/2+Func3(13)/2+ Func3(21)/2):f2}");
            Console.WriteLine("Enter number");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Otvet = {Func4(a)}");
            Console.WriteLine("Enter number");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Otvet = {Func5(a)}");

            Console.WriteLine("Enter a,b and h");
            double aa = Convert.ToDouble(Console.ReadLine());
            double bb = Convert.ToDouble(Console.ReadLine());
            double h = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Tablica 1");
            for (double i = aa; i <= bb; i += h)
            {
                Console.WriteLine($"X = {i} | Y = {FuncOne(i):f2}");
            }
            Console.WriteLine("Tablica 2");
            for (double i = aa; i <= bb; i += h)
            {
                Console.WriteLine($"X = {i} | Y = {FuncTwo(i):f2}");
            }
            Console.WriteLine("Tablica 3");
            for (double i = aa; i <= bb; i += h)
            {
                Console.WriteLine($"X = {i} | Y = {FuncThree(i,aa):f2}");
            }
            Console.WriteLine("Tablica 4");
            for (double i = aa; i <= bb; i += h)
            {
                Console.WriteLine($"X = {i} | Y = {FuncFour(i):f2}");
            }
        }
    }
}
