﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace LabRabota16
{
    class Person: IComparable
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string patronymic { get; set; }
        public int borndate { get; set; }
        public string CityBorn { get; set; }
        public string school { get; set; }
        public Person(string name,string surname,string patronymic,int borndate,string CityBorn,string school)
        {
            this.name = name;
            this.surname = surname;
            this.patronymic = patronymic;
            this.borndate = borndate;
            this.CityBorn = CityBorn;
            this.school = school;
        }
        public string ToString()
        {
            return ($"{this.name} {this.surname} {this.patronymic} {this.borndate} {this.CityBorn} {this.school}");
        }
        public int CompareTo(object obj)
        {
            Person p = obj as Person;
            if (p != null)
            {
                if (this.borndate < p.borndate)
                {
                    return -1;
                }
                else if (this.borndate > p.borndate) return 1;
                else return 0;
            }
            else 
            {
                throw new Exception("This is not Person");
            }
        }
    }
    class Students : IComparable
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string patronymic { get; set; }
        public int numbergroup { get; set; }
        public int threeexams { get; set; }
        public Students(string name,string surname, string patronymic, int numbergroup,int threeexams)
        {
            this.name = name;
            this.surname = surname;
            this.patronymic = patronymic;
            this.numbergroup = numbergroup;
            this.threeexams = threeexams;
        }
        public int CompareTo(object obj)
        {
            Students p = obj as Students;
            if (p != null)
            {
                if (this.numbergroup < p.numbergroup)
                {
                    return -1;
                }
                else if (this.numbergroup > p.numbergroup) return 1;
                else return 0;
            }
            else
            {
                throw new Exception("This is not student");
            }
        }
        public bool CheckExams()
        {
            if (threeexams > 200) return true;
            else return false;
        }
        public string ToString()
        {
            return ($"{this.name} {this.surname} {this.patronymic} {this.numbergroup} {this.threeexams}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Write number of task(1 or 2)");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task)
            {
                case 1:
                    string[] Personsstr = File.ReadAllLines(@".\input.txt");
                    Person[] Persons = new Person[Personsstr.Length];
                    for(int i = 0;i<Personsstr.Length;i++)
                    {
                        int j = 0;
                        string name = "";
                        while (Personsstr[i][j] != Convert.ToChar(" "))
                        {
                            name += Personsstr[i][j];
                            j++;
                        }
                        j++;
                        string surname = "";
                        while (Personsstr[i][j] != Convert.ToChar(" "))
                        {
                            surname += Personsstr[i][j];
                            j++;
                        }
                        j++;
                        string patronymic = "";
                        while (Personsstr[i][j] != Convert.ToChar(" "))
                        {
                            patronymic += Personsstr[i][j];
                            j++;
                        }
                        j++;
                        string borndatestr = "";
                        while (Personsstr[i][j] != Convert.ToChar(" "))
                        {
                            borndatestr += Personsstr[i][j];
                            j++;
                        }
                        int borndate = Convert.ToInt32(borndatestr);
                        j++;
                        string CityBorn = "";
                        while (Personsstr[i][j] != Convert.ToChar(" "))
                        {
                            CityBorn += Personsstr[i][j];
                            j++;
                        }
                        j++;
                        string school = "";
                        while (j != Personsstr[i].Length)
                        {
                            school += Personsstr[i][j];
                            j++;
                        }
                        Persons[i] = new Person(name,surname,patronymic,borndate,CityBorn,school);
                    }
                    Array.Sort(Persons);
                    var output = new StreamWriter(@".\output.txt");
                    for (int i = 0; i < Personsstr.Length; i++)
                    {
                        output.WriteLine(Persons[i].ToString());
                        
                    }
                    output.Close();
                        break;
                case 2:
                    string[] Studentsstr = File.ReadAllLines(@".\input2.txt");
                    Students[] Students = new Students[Studentsstr.Length];
                    for (int i = 0; i < Studentsstr.Length; i++)
                    {
                        int j = 0;
                        string name = "";
                        while (Studentsstr[i][j] != Convert.ToChar(" "))
                        {
                            name += Studentsstr[i][j];
                            j++;
                        }
                        j++;
                        string surname = "";
                        while (Studentsstr[i][j] != Convert.ToChar(" "))
                        {
                            surname += Studentsstr[i][j];
                            j++;
                        }
                        j++;
                        string patronymic = "";
                        while (Studentsstr[i][j] != Convert.ToChar(" "))
                        {
                            patronymic += Studentsstr[i][j];
                            j++;
                        }
                        j++;
                        string numbergroupstr = "";
                        while (Studentsstr[i][j] != Convert.ToChar(" "))
                        {
                            numbergroupstr += Studentsstr[i][j];
                            j++;
                        }
                        int numbergroup = Convert.ToInt32(numbergroupstr);
                        j++;
                        string threeexamsstr = "";
                        while (j != Studentsstr[i].Length)
                        {
                            threeexamsstr += Studentsstr[i][j];
                            j++;
                        }
                        int threeexams = Convert.ToInt32(threeexamsstr);
                        Students[i] = new Students(name, surname, patronymic, numbergroup,threeexams);
                    }
                        Array.Sort(Students);
                    var output2 = new StreamWriter(@".\output2.txt");
                    for (int i = 0; i < Studentsstr.Length; i++)
                    {
                        if(Students[i].CheckExams())
                        output2.WriteLine(Students[i].ToString());
                    }
                    output2.Close();
                    break;
            }
        }
    }
}
