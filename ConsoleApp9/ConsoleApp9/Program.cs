﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of task");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task)
            {
                case 1:
                    Console.WriteLine("Write anything");
                    string str = Console.ReadLine();
                    char[] sym = str.ToCharArray();
                    for (int i = 0; i <= str.Length - 1; i += 2)
                    {
                        char c = sym[i];
                        sym[i] = sym[i + 1];
                        sym[i + 1] = c;
                    }
                    str = new string(sym);
                    Console.WriteLine(str);
                    break;
                case 2:
                    int countletters = 0;
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    sym = str.ToCharArray();
                    for (int i = 0; i <= str.Length - 1; i++)
                    {
                        if (char.IsLetter(sym[i])) ++countletters;
                    }
                    Console.WriteLine($"Count of letters = {countletters}");
                    break;
                case 3:
                    bool twos = false;
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    sym = str.ToCharArray();
                    for (int i = 0; i <= str.Length - 2; i++)
                    {
                        if (sym[i] == sym[++i]) twos = true;
                    }
                    if (twos) Console.WriteLine("Yes, two the same symbols");
                    else Console.WriteLine("No");
                    break;
                case 4:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    if (str.Length%2 == 0)
                    {
                        Console.WriteLine(str.Remove(str.Length/2-1,2));
                    }
                    else
                    {
                        Console.WriteLine(str.Remove(str.Length / 2 , 1));
                    }
                    break;
                case 5:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    Console.WriteLine("Write substr1");
                    string substr1 = Console.ReadLine();
                    Console.WriteLine("Write substr2");
                    string substr2 = Console.ReadLine();
                    Console.WriteLine(str.Replace(substr1, substr2));
                    break;
                case 6:
                    int summ = 0;
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    sym = str.ToCharArray();
                    for(int i = 0; i <= str.Length -1; i++)
                    {
                        if (char.IsDigit(sym[i]))
                        {
                            char tch = sym[i];
                            string strtch = Convert.ToString(tch);
                            int integer = Convert.ToInt32(strtch);
                            summ = summ + integer;
                        }
                    }
                    Console.WriteLine($"Summa = {summ}");
                    break;
                case 7:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string[] parts = str.Split(':');
                    Console.WriteLine(parts[0]);
                    break;
                case 8:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    parts = str.Split(':');
                    Console.WriteLine(parts[parts.Length-1]);
                    break;
                case 9:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    parts = str.Split(',');
                    Console.WriteLine(parts[0]+parts[2]);
                    break;
                case 10:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    int dlstr = str.Length;
                    int raz = dlstr;
                    int shozh = 0;
                    sym = str.ToCharArray();
                    for (int i = 0; i<= dlstr - 2; i++)
                    {
                        int count = 0;
                        for(int j = i+1; j <= dlstr - 1; j++)
                        {
                            if (sym[i]== sym[j])
                            {
                                raz = raz - 1;
                                ++count;
                                if (count > 1)
                                {
                                    shozh++;
                                }
                            }
                        }
                    }
                    Console.WriteLine($"Count of diffrent symbols = {raz+shozh}");
                    break;
                default:
                    Console.WriteLine("Task doesn't exist");
                    break;
            }

        }
    }
}
