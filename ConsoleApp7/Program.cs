﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static bool IsPalindrom(string k)
        {
            int f = 1;
            int j = k.Length;
            int ch = Convert.ToInt32(k);
            for (int i = 0; i < j; i++)
            {
                if (k[i] == k[j - i])
                {
                    continue;
                }
                else
                {
                    f = 0;
                }
            }
            if (f == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        static void SortInc3(ref int a, ref int b, ref int c)
        {
            if (a <= b && a <= c)
            {
                if (b <= c)
                {
                    Console.WriteLine($"{a} , {b} , {c}");
                }
                else { Console.WriteLine($"{a} , {c} , {b}"); }
            }
            else if (b <= a && b <= c)
            {
                if (a <= c)
                {
                    Console.WriteLine($"{b} , {a} , {c}");
                }
                else { Console.WriteLine($"{b} , {c} , {a}"); }
            }
            else if (c <= a && c <= b)
            {
                if (a <= b)
                {
                    Console.WriteLine($"{c} , {a} , {b}");
                }
                else { Console.WriteLine($"{c} , {b} , {a}"); }
            }
        }
        static bool IsSquare(double x)
        {
            if (Math.Pow(x, 0.5) % 1 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Task1");
            int count = 0;
            Console.WriteLine("Enter numbers");
            for (int i = 1; i <= 10; ++i)
            {
                double x = Convert.ToDouble(Console.ReadLine());
                if (IsSquare(x))
                {
                    ++count;
                }
            }
            Console.WriteLine($"Count = {count}");
            Console.WriteLine("Task 2");
            Console.WriteLine("Enter first 3 numbers");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            SortInc3(ref a, ref b, ref c);
            Console.WriteLine("Enter second 3 numbers");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());
            SortInc3(ref a, ref b, ref c);
            Console.WriteLine("Task 3");
            for (int i = 1; i <= 10; i++)
            {
                string p = Console.ReadLine();
                if (IsPalindrom(p))
                {
                    Console.WriteLine("Is palindrom");
                }
                else
                {
                    Console.WriteLine("Is not palindrom");
                }
            }
        }
    }
}
