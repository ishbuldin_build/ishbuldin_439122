﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LabRabota13
{
    abstract class Figure
    {
        public double Perimeter;
        public double Area;
        public abstract void Enter();
        public abstract void PerimeterF();
        public abstract void AreaF();
        public abstract void Print();
    } 
    class Rectangle : Figure
    {
        public  double length { get; set; }
        public  double width { get; set; }
        public override void Enter()
        {
            Console.WriteLine("Enter length");
            length = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter width");
            width = Convert.ToDouble(Console.ReadLine());
        }
        public override void PerimeterF()
        {
            Perimeter = length+width;
        }
        public override void AreaF()
        {
            Area = length*width;
        }
        public override void Print()
        {
            Console.WriteLine($"Figure - Rectangle");
            Console.WriteLine($"Perimeter - {Perimeter:f2}");
            Console.WriteLine($"Area = {Area:f2}");
        }
    }
    class Circle : Figure
    {

        public double r { get; set; }
        public override void Enter()
        {
            Console.WriteLine("Enter Radius");
            r = Convert.ToDouble(Console.ReadLine());
        }
        public override void PerimeterF()
        {
            Perimeter = 2*Math.PI*r;
        }
        public override void AreaF()
        {
            Area = Math.PI*Math.Pow(r,2);
        }
        public override void Print()
        {
            Console.WriteLine($"Figure - Circle");
            Console.WriteLine($"Perimeter - {Perimeter:f2}");
            Console.WriteLine($"Area = {Area:f2}");
        }
    }
    class Triangle : Figure
    {
        public double a { get; set; }
        public double b { get; set; }
        public double c { get; set; }
        public override void Enter()
        {
            Console.WriteLine("Enter side 1");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter side 2");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter side 3");
            c = Convert.ToDouble(Console.ReadLine());
        }
        public override void PerimeterF()
        {
            Perimeter = a+b+c;
        }
        public override void AreaF()
        {
            Area = Math.Pow(Perimeter*((Perimeter-a)*(Perimeter - b) *(Perimeter - c)),0.5);
        }
        public override void Print()
        {
            Console.WriteLine($"Figure - Triangle");
            Console.WriteLine($"Perimeter - {Perimeter:f2}");
            Console.WriteLine($"Area = {Area:f2}");
        }
    }
    abstract class Function
    {
        public double y { get; set; }
        public double x { get; set; }
        public abstract void Enter();
        public abstract void Print();
        public abstract void Yfinder();
    }
    class Line : Function
    {
        public double a;
        public double b;
        public override void Enter()
        {
            Console.WriteLine("Function line - y = a*x + b");
            Console.WriteLine("Enter a");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter b");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter x");
            x = Convert.ToDouble(Console.ReadLine());
        }
        public override void Yfinder()
        {
            y = a*x+b;
        }
        public override void Print()
        {
            Console.WriteLine($"y = {a} * x + {b}");
            Console.WriteLine($"Where x if {x}");
            Console.WriteLine($"y = {y}");
        }
    }
    class Kub : Function
    {
        public double a;
        public double b;
        public double c;
        public override void Enter()
        {
            Console.WriteLine("Function kub - y = a*x^2 + b*x + c");
            Console.WriteLine("Enter a");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter b");
            b= Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter c");
            c = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter x");
            x = Convert.ToDouble(Console.ReadLine());
        }
        public override void Yfinder()
        {
            y = a * Math.Pow(x,2) + b*x+c;
        }
        public override void Print()
        {
            Console.WriteLine($"y = {a} * x^2 + {b} * x + {c}");
            Console.WriteLine($"Where x if {x}");
            Console.WriteLine($"y = {y}");
        }
    }
    class Hyperbola : Function
    {
        public double a;
        public double b;
        public override void Enter()
        {
            Console.WriteLine("Function hyperbola - y = a/x + b");
            Console.WriteLine("Enter a");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter b");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter x");
            x = Convert.ToDouble(Console.ReadLine());
        }
        public override void Yfinder()
        {
            y = a/x+b;
        }
        public override void Print()
        {
            Console.WriteLine($"y = {a} / x + {b}");
            Console.WriteLine($"Where x if {x}");
            Console.WriteLine($"y = {y}");
        }
    }
    abstract class Edition
    {
        public string surname;
        public string title;
        public abstract void Enter();
        public abstract bool IsThatAuthor(string sf);
        public abstract void Print();
    }
    class Book : Edition
    {
        public int yearedit;
        public string editiontitle;
        public override void Enter()
        {
            Console.WriteLine("Write title of book");
            title = Console.ReadLine();
            Console.WriteLine("Write surname of author");
            surname = Console.ReadLine();
            Console.WriteLine("Write year edit");
            yearedit = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write title of Edition");
            editiontitle = Console.ReadLine();
        }
        public override bool IsThatAuthor(string sf)
        {
            if (surname == sf) return true;
            else return false;
        }
        public override void Print()
        {
            Console.WriteLine();
            Console.WriteLine("BOOK");
            Console.WriteLine($"Title - {title}");
            Console.WriteLine($"Surname of author  - {surname}");
            Console.WriteLine($"Year edit - {yearedit}");
            Console.WriteLine($"Title of Edition - {editiontitle}");
        }
    }
    class Article : Edition
    {
        public int yearedit;
        public int numberedition;
        public string magazinename;
        public override void Enter()
        {
            Console.WriteLine("Write title of Article");
            title = Console.ReadLine();
            Console.WriteLine("Write surname of author");
            surname = Console.ReadLine();
            Console.WriteLine("Write title of magazine");
            magazinename = Console.ReadLine();
            Console.WriteLine("Write number of edition");
            numberedition = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write year edit");
            yearedit = Convert.ToInt32(Console.ReadLine());
        }
        public override bool IsThatAuthor(string sf)
        {
            if (surname == sf) return true;
            else return false;
        }
        public override void Print()
        {
            Console.WriteLine();
            Console.WriteLine("Article");
            Console.WriteLine($"Title - {title}");
            Console.WriteLine($"Surname of author  - {surname}");
            Console.WriteLine($"Title of magazine- {magazinename}");
            Console.WriteLine($"Number of edition - {numberedition}");
            Console.WriteLine($"Year edit - {yearedit}");
        }
    }
    class Eresource : Edition
    {
        public string url;
        public string annotations;
        public override void Enter()
        {
            Console.WriteLine("Write title of Article");
            title = Console.ReadLine();
            Console.WriteLine("Write surname of author");
            surname = Console.ReadLine();
            Console.WriteLine("Write url");
            url = Console.ReadLine();
            Console.WriteLine("Write annotations");
            annotations = Console.ReadLine();
        }
        public override bool IsThatAuthor(string sf)
        {
            if (surname == sf) return true;
            else return false;
        }
        public override void Print()
        {
            Console.WriteLine();
            Console.WriteLine("Electronic resource");
            Console.WriteLine($"Title - {title}");
            Console.WriteLine($"Surname of author  - {surname}");
            Console.WriteLine($"URL - {url}");
            Console.WriteLine($"Annotations - {annotations}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of task");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task) 
            {
                case 1:
                    int choose;
                    Console.WriteLine("Enter counts of figures");
                    int n = Convert.ToInt32(Console.ReadLine());
                    Figure[] figures = new Figure[n];
                    for(int i = 0; i < n; i++)
                    {
                        Console.WriteLine("Switch,if figure is Rectangle - 1,if Circle - 2,if Triangle - 3");
                        choose = Convert.ToInt32(Console.ReadLine());
                        switch (choose)
                        {
                            case 1:
                                Rectangle rectangle = new Rectangle();
                                rectangle.Enter();
                                figures[i] = rectangle;
                                break;
                            case 2:
                                Circle circle = new Circle();
                                circle.Enter();
                                figures[i] = circle;
                                break;
                            case 3:
                                Triangle triangle = new Triangle();
                                triangle.Enter();
                                figures[i] = triangle;
                                break;
                            default:
                                Console.WriteLine("Wrong figure(1,2 or 3)");
                                break;
                        }
                    }
                    Console.WriteLine("Print:");
                    for (int i = 0; i < n; i++)
                    {
                        figures[i].AreaF();
                        figures[i].PerimeterF();
                        figures[i].Print();
                    }
                    break;
                case 2:
                    Console.WriteLine("Enter counts of functions");
                    n = Convert.ToInt32(Console.ReadLine());
                    Function[] functions = new Function[n];
                    for (int i = 0; i < n; i++)
                    {
                        Console.WriteLine("Switch,if function is Line - 1,if Kub - 2,if Hyberbola - 3");
                        choose = Convert.ToInt32(Console.ReadLine());
                        switch (choose)
                        {
                            case 1:
                                Line line = new Line();
                                line.Enter();
                                functions[i] = line;
                                break;
                            case 2:
                                Kub kub = new Kub();
                                kub.Enter();
                                functions[i] = kub;
                                break;
                            case 3:
                                Hyperbola hyperbola = new Hyperbola();
                                hyperbola.Enter();
                                functions[i] = hyperbola;
                                break;
                            default:
                                Console.WriteLine("Wrong functon(1,2 or 3)");
                                break;
                        }
                    }
                    Console.WriteLine("Print:");
                    for (int i = 0; i < n; i++)
                    {
                        Console.WriteLine();
                        functions[i].Yfinder();
                        functions[i].Print();
                    }
                    break;
                case 3:
                    Console.WriteLine("Enter counts of Editions");
                    n = Convert.ToInt32(Console.ReadLine());
                    Edition[] editions = new Edition[n];
                    for (int i = 0; i < n; i++)
                    {
                        Console.WriteLine("Switch,if edition is Book - 1,if Article - 2,if Electronic resource - 3");
                        choose = Convert.ToInt32(Console.ReadLine());
                        switch (choose)
                        {
                            case 1:
                                Book book = new Book();
                                book.Enter();
                                editions[i] = book;
                                break;
                            case 2:
                                Article article = new Article();
                                article.Enter();
                                editions[i] = article;
                                break;
                            case 3:
                                Eresource eresource = new Eresource();
                                eresource.Enter();
                                editions[i] = eresource;
                                break;
                            default:
                                Console.WriteLine("Wrong edition(1,2 or 3)");
                                break;
                        }
                    }
                    Console.WriteLine("Print:");
                    for (int i = 0; i < n; i++)
                    {
                        editions[i].Print();
                    }
                    Console.WriteLine("Write surname to find his edition");
                    string sf = Console.ReadLine();
                    bool nhs = true;
                    Console.WriteLine("Answer:");
                    for (int i = 0; i < n; i++)
                    {
                        if (editions[i].IsThatAuthor(sf)) { editions[i].Print(); nhs = false; }
                    }
                    if (nhs) Console.WriteLine("We can't found efition of this sirname");
                    break;
                default:
                    Console.WriteLine("Wrong number of task");
                    break;
            }
        }
    }
}
