﻿using System;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.InteropServices;

namespace LabRabota15
{
    interface Number
    {
        string numberstring { get; set; }
        long numberlong { get; set; }
        Number add(Number n);
        Number sub(Number n);
        int compareTo(Number n);
    }
    class NotNaturalNumberException : Exception
    {
        public string numberstring { get; set; }
        public long numberlong { get;  set; }
    }
    class SimpleLongNumber:Number
    {
        public long numberlong { get; set; }
        public string numberstring { get; set; }
        public Number add(Number n)
        {
            Number add = new SimpleLongNumber();
            add.numberlong = this.numberlong + n.numberlong;
            return add;
        }
        public Number sub(Number n)
        {
            Number sub = new SimpleLongNumber();
            if (this.numberlong > n.numberlong) sub.numberlong = this.numberlong - n.numberlong;
            else
            {
                NotNaturalNumberException numberlong = new NotNaturalNumberException();
                sub.numberlong = this.numberlong - n.numberlong;
                numberlong.numberlong = sub.numberlong;
            }
            return sub;
        }
        public int compareTo(Number n)
        {
            if (this.numberlong > n.numberlong) return 1;
            else if (this.numberlong == n.numberlong) return 0;
            else return -1;
        }
    }
    class VeryLongNumber:Number
    {
        public long numberlong { get; set; }
        public string numberstring { get; set; }
        public Number add(Number n)
        {
            Number add = new VeryLongNumber();
            int help = 0;
            if (numberstring.Length < n.numberstring.Length)
            {
                this.numberstring.Reverse();
                n.numberstring.Reverse();
                for (int i = 0; i < this.numberstring.Length; i++)
                {
                    if (n.numberstring[i] + this.numberstring[i] > 9)
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 1;
                    }
                    else
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 0;
                    }
                }
                for (int i = this.numberstring.Length; i <= n.numberstring.Length; i++)
                {
                    add.numberstring += n.numberstring[i]+help;
                    help = 0;
                }
                add.numberstring.Reverse();
            }
            else if (this.numberstring.Length == n.numberstring.Length)
            {
                this.numberstring.Reverse();
                n.numberstring.Reverse();
                for (int i = 0; i < n.numberstring.Length; i++)
                {
                    if (n.numberstring[i] + this.numberstring[i] > 9)
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 1;
                    }
                    else
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 0;
                    }
                }           
                if(n.numberstring[n.numberstring.Length] + this.numberstring[n.numberstring.Length] > 9)
                {
                    add.numberstring += n.numberstring[n.numberstring.Length] + help + this.numberstring[n.numberstring.Length];
                    add.numberstring += 1;
                }
                    add.numberstring.Reverse();
            }
            else
            {
                this.numberstring.Reverse();
                n.numberstring.Reverse();
                for (int i = 0; i < n.numberstring.Length; i++)
                {
                    if (this.numberstring[i] + n.numberstring[i] > 9)
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 1;
                    }
                    else
                    {
                        add.numberstring += n.numberstring[i] + help + this.numberstring[i];
                        help = 0;
                    }
                }
                for (int i = n.numberstring.Length; i <= this.numberstring.Length; i++)
                {
                    add.numberstring += this.numberstring[i] + help;
                    help = 0;
                }
                add.numberstring.Reverse();
            }
            return add;
        }
        public Number sub(Number n)
        {
            Number sub = new VeryLongNumber();
            int help = 0;
            bool bigger = true;
            if ((this.numberstring.Length < n.numberstring.Length))
            {
                this.numberstring.Reverse();
                n.numberstring.Reverse();
                for(int i = 0; i < this.numberstring.Length; i++)
                {
                    if (n.numberstring[i] < this.numberstring[i])
                    {
                        sub.numberstring += n.numberstring[i] - help + 10 - this.numberstring[i];
                        help = 1;
                    }
                    else
                    {
                        sub.numberstring += n.numberstring[i] - help - this.numberstring[i];
                        help = 0;
                    }
                }
                if (n.numberstring[this.numberstring.Length-1] > this.numberstring[this.numberstring.Length-1])
                {
                    sub.numberstring += n.numberstring[this.numberstring.Length] - 1;
                }else sub.numberstring += n.numberstring[this.numberstring.Length];
                for (int i = this.numberstring.Length + 1; i <= n.numberstring.Length; i++)
                {
                    sub.numberstring += n.numberstring[i];
                }
                sub.numberstring += "-";
                sub.numberstring.Reverse();
                NotNaturalNumberException numberstring = new NotNaturalNumberException();
                numberstring.numberstring = sub.numberstring;
            }
            else if (this.numberstring.Length == n.numberstring.Length)
            {
                for (int i = 0; i < n.numberstring.Length; i++)
                {
                    if (this.numberstring[i] < n.numberstring[i]) bigger = false;break;
                }
                if (bigger)
                {
                    this.numberstring.Reverse();
                    n.numberstring.Reverse();
                    for (int i = 0; i < this.numberstring.Length; i++)
                    {
                        if (this.numberstring[i] < n.numberstring[i])
                        {
                            sub.numberstring += this.numberstring[i] - help + 10 - n.numberstring[i];
                            help = 1;
                        }
                        else
                        {
                            sub.numberstring += this.numberstring[i] - help - n.numberstring[i];
                            help = 0;
                        }
                    }
                    sub.numberstring.Reverse();
                } else
                {
                    for (int i = 0; i < this.numberstring.Length; i++)
                    {
                        if (n.numberstring[i] < this.numberstring[i])
                        {
                            sub.numberstring += n.numberstring[i] - help + 10 - this.numberstring[i];
                            help = 1;
                        }
                        else
                        {
                            sub.numberstring += n.numberstring[i] - help - this.numberstring[i];
                            help = 0;
                        }
                    }
                    sub.numberstring.Reverse();
                }
            }
            else
            {
                this.numberstring.Reverse();
                n.numberstring.Reverse();
                for (int i = 0; i < n.numberstring.Length; i++)
                {
                    if (this.numberstring[i] < n.numberstring[i])
                    {
                        sub.numberstring += this.numberstring[i] - help + 10 - n.numberstring[i];
                        help = 1;
                    }
                    else
                    {
                        sub.numberstring += this.numberstring[i] - help - n.numberstring[i];
                        help = 0;
                    }
                }
                if (this.numberstring[n.numberstring.Length - 1] > n.numberstring[n.numberstring.Length - 1])
                {
                    sub.numberstring += this.numberstring[n.numberstring.Length] - 1;
                }
                else sub.numberstring += this.numberstring[n.numberstring.Length];
                for (int i = n.numberstring.Length + 1; i <= this.numberstring.Length; i++)
                {
                    sub.numberstring += this.numberstring[i];
                }
                sub.numberstring.Reverse();
            }
            return sub;
        }
        public int compareTo(Number n)
        {
            int anser = 0;
            if (this.numberstring.Length > n.numberstring.Length) anser = 1;
            else if (this.numberstring.Length == n.numberstring.Length)
            {
                for(int i = 0; i < this.numberstring.Length; i++)
                {
                    if (this.numberstring[i] < n.numberstring[i])
                    {
                        anser = -1; break;
                    } else if (this.numberstring[i] > n.numberstring[i]) anser = 1; break;
                }
            }
            else if (this.numberstring.Length < n.numberstring.Length) anser = -1;
            return anser;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Number[] numbers = new Number[4];
            numbers[0] = new VeryLongNumber();
            numbers[0].numberstring = "1321561125215213265";
            numbers[1] = new SimpleLongNumber();
            numbers[1].numberlong = 132;
            numbers[1].numberstring = $"{numbers[1].numberlong}";
            numbers[2] = new VeryLongNumber();
            numbers[2].numberstring = "999999999999";
            numbers[3] = new SimpleLongNumber();
            numbers[3].numberlong = 8885858888;
            numbers[3].numberstring = $"{numbers[3].numberlong}";
            Console.WriteLine("Number 1 = 1321561125215213265");
            Console.WriteLine("Number 2 = 132");
            Console.WriteLine("Number 3 = 999999999999");
            Console.WriteLine("Number 4 = 8885858888");
            for(int i = 0; i < 3; i++)
            {
                numbers[i] = new VeryLongNumber();
                numbers[i] = numbers[i].add(numbers[i+1]);
            }
            Console.WriteLine($"Summa = {numbers[3].numberstring}");
        }
    }
}
