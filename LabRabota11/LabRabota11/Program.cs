﻿using System;

namespace LabRabota11
{
    class Vector2D
    {
        public double x { get; set; }
        public double y { get; set; }
        public Vector2D()
        {
            x = 0;
            y = 0;
        }
        public Vector2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        public Vector2D add( Vector2D Vector1 )
        {
            Vector2D vector = new Vector2D();
            vector.x = this.x + Vector1.x;
            vector.y = this.y + Vector1.y;
            return vector;
        }
        public void add2(Vector2D Vector1)
        {
            this.x = this.x + Vector1.x;
            this.y = this.y + Vector1.y;
        }
        public Vector2D sub(Vector2D Vector1)
        {
            Vector2D vector = new Vector2D();
            vector.x = this.x - Vector1.x;
            vector.y = this.y - Vector1.y;
            return vector;
        }
        public void sub2(Vector2D Vector1)
        {
            this.x = this.x - Vector1.x;
            this.y = this.y - Vector1.y;
        }
        public Vector2D mult(double a)
        {
            Vector2D vector = new Vector2D();
            vector.x = this.x * a;
            vector.y = this.y * a;
            return vector;
        }
        public void mult2(double a)
        {
            this.x = this.x * a;
            this.y = this.y * a;
        }
        public string ToString()
        {
            return $"({this.x};{this.y})";
        }
        public double lenght()
        {
            double lenght = Math.Pow(Math.Pow(this.x, 2) + Math.Pow(this.y, 2), 0.5);
            return lenght;
        }
        public double scalarProduct(Vector2D vector)
        {
            double scalarProduct = (this.x * vector.x) + (this.y * vector.y);
            return scalarProduct;
        }
        public double cos(Vector2D vector)
        {
            double cos = (this.x*vector.x + this.y*vector.y) / (Math.Pow(Math.Pow(this.x, 2) + Math.Pow(this.y, 2), 0.5) * Math.Pow(Math.Pow(vector.x, 2) + Math.Pow(vector.y, 2), 0.5));
            return cos;
        }
        public bool equals(Vector2D vector)
        {
            if (Math.Pow(Math.Pow(this.x, 2) + Math.Pow(this.y, 2), 0.5) == Math.Pow(Math.Pow(vector.x, 2) + Math.Pow(vector.y, 2), 0.5))
                {
                return true;
            } else return false;
        }
    }
    class RationalFraction
    {
        public int up { get; set; }
        public int low { get; set; }
        public RationalFraction()
        {
            this.up = 0;
            this.low = 1;
        }
        public RationalFraction(int up,int low)
        {
            this.up = up;
            this.low = low;
        }
        public void reduce()
        {
            int a = this.up;
            int b = this.low;
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }
            this.up = this.up / a;
            this.low = this.low / a;
            if (this.up>=0 && this.low < 0)
            {
                this.up = -this.up;
                this.low *= (-1);
            }
            if (this.up < 0 && this.low < 0)
            {
                this.up *= (-1);
                this.low *= (-1);
            }
        }
        public RationalFraction add(RationalFraction fraction2)
        {
            RationalFraction fraction = new RationalFraction();
            fraction.up = this.up*fraction2.low + fraction2.up*this.low;
            fraction.low = this.low*fraction2.low;
            fraction.reduce();
            return fraction;
        }
        public void add2(RationalFraction fraction2)
        {
            this.up = this.up * fraction2.low + fraction2.up * this.low;
            this.low = this.low * fraction2.low;
        }
        public RationalFraction sub(RationalFraction fraction2)
        {
            RationalFraction fraction = new RationalFraction();
            fraction.up = this.up * fraction2.low - fraction2.up * this.low;
            fraction.low = this.low * fraction2.low;
            fraction.reduce();
            return fraction;
        }
        public void sub2(RationalFraction fraction2)
        {
            this.up = this.up * fraction2.low - fraction2.up * this.low;
            this.low = this.low * fraction2.low;
        }
        public RationalFraction mult(int factor)
        {
            RationalFraction fraction = new RationalFraction();
            fraction.up = this.up * factor;
            fraction.low = this.low;
            fraction.reduce();
            return fraction;
        }
        public void mult2(int factor)
        {
            this.up = this.up * factor;
        }
        public RationalFraction div(RationalFraction fraction2)
        {
            RationalFraction fraction = new RationalFraction();
            fraction.up = this.up * fraction2.low;
            fraction.low = this.low * fraction2.up;
            fraction.reduce();
            return fraction;
        }
        public void div2(RationalFraction fraction2)
        {
            this.up = this.up * fraction2.low;
            this.low = this.low * fraction2.up;
        }
        public string toString()
        {
            return $" {this.up}/{this.low} ";
        }
        public double value()
        {
            return this.up / this.low;
        }
        public bool equals(RationalFraction fraction2)
        {
            if ((this.up == fraction2.up) && (this.low == fraction2.low)) return true; else return false;
        }
        public int numberPart()
        {
            return Math.Abs(this.up / this.low);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number task");
            int Task = Convert.ToInt32(Console.ReadLine());
            switch (Task)
            {
                case 1:
                    Vector2D VectorNull = new Vector2D();
                    Console.WriteLine($"Null Vector = {VectorNull.ToString()}");
                    Console.WriteLine("Enter X and Y for Vector1");
                    double x = Convert.ToDouble(Console.ReadLine());
                    double y = Convert.ToDouble(Console.ReadLine());
                    Vector2D Vector1 = new Vector2D(x, y);
                    Console.WriteLine($"Vector1 = {Vector1.ToString()}");
                    Console.WriteLine("Enter X and Y for Vector2");
                    x = Convert.ToDouble(Console.ReadLine());
                    y = Convert.ToDouble(Console.ReadLine());
                    Vector2D Vector2 = new Vector2D(x, y);
                    Console.WriteLine($"Vector2 = {Vector2.ToString()}");
                    Vector2D VectorAdd = Vector2.add(Vector1);
                    Console.WriteLine($"Vector1 + Vector2 = {VectorAdd.ToString()}");
                    Vector2.add2(Vector1);
                    Console.WriteLine($"Vector2 = Vector1 + Vector2 = {Vector2.ToString()}");
                    Vector2D VectorSub = Vector2.sub(Vector1);
                    Console.WriteLine($"Vector2 - Vector1 = {VectorSub.ToString()}");
                    Vector2.sub2(Vector1);
                    Console.WriteLine($"Vector2 = Vector2 - Vector1 = {Vector2.ToString()}");
                    Console.WriteLine("Enter Factor");
                    double factor = Convert.ToDouble(Console.ReadLine());
                    Vector2D VectorMult = Vector1.mult(factor);
                    Console.WriteLine($"Vector1 * Factor = {VectorMult.ToString()}");
                    Vector1.mult2(factor);
                    Console.WriteLine($"Vector1 = Vector1 * Factor = {Vector1.ToString()}");
                    Console.WriteLine($"lenght of Vector2 = {Vector2.lenght()}");
                    Console.WriteLine($"ScalarProduct = {Vector1.scalarProduct(Vector2)}");
                    Console.WriteLine($"Cosinus of Corner between Vector1 and Vector2 = {Vector1.cos(Vector2)}");
                    Console.WriteLine($"Are vectors same? - {Vector1.equals(Vector2)}");
                    break;
                case 2:
                    RationalFraction FractionNull = new RationalFraction();
                    Console.WriteLine($"FractionNull = {FractionNull.toString()}");
                    Console.WriteLine("Enter Fraction1,Numerator then Denominator ");
                    int up = Convert.ToInt32(Console.ReadLine());
                    int low = Convert.ToInt32(Console.ReadLine());
                    RationalFraction Fraction1 = new RationalFraction(up,low);
                    Console.WriteLine($"Fraction1 = {Fraction1.toString()}");
                    Fraction1.reduce();
                    Console.WriteLine($"Fraction1 after reduction = {Fraction1.toString()}");
                    Console.WriteLine("Enter Fraction2,Numerator then Denominator ");
                    up = Convert.ToInt32(Console.ReadLine());
                    low = Convert.ToInt32(Console.ReadLine());
                    RationalFraction Fraction2 = new RationalFraction(up, low);
                    Fraction2.reduce();
                    Console.WriteLine($"Fraction2 = {Fraction2.toString()}");
                    RationalFraction FractionAdd = Fraction1.add(Fraction2);
                    Console.WriteLine($"FractionAdd = Fraction1 + Fraction2 = {FractionAdd.toString()}");
                    Fraction1.add2(Fraction2);
                    Fraction1.reduce();
                    Console.WriteLine($"Fraction1 = Fraction1 + Fraction2 = {Fraction1.toString()}");
                    RationalFraction FractionSub = Fraction1.sub(Fraction2);
                    Console.WriteLine($"FractionSub = Fraction1 - Fraction2 = {FractionSub.toString()}");
                    Fraction1.sub2(Fraction2);
                    Fraction1.reduce();
                    Console.WriteLine($"Fraction1 = Fraction1 - Fraction2 = {Fraction1.toString()}");
                    Console.WriteLine("Enter Factor");
                    int factor2 = Convert.ToInt32(Console.ReadLine());
                    RationalFraction FractionMult = Fraction1.mult(factor2);
                    Console.WriteLine($"FractionMult = Fraction1 * Factor = {FractionMult.toString()}");
                    Fraction1.mult2(factor2);
                    Fraction1.reduce();
                    Console.WriteLine($"Fraction1 = Fraction1 * Factor = {Fraction1.toString()}");
                    RationalFraction FractionDiv = Fraction1.div(Fraction2);
                    Console.WriteLine($"FractionDiv = Fraction1/Fraction2 = {FractionDiv.toString()}");
                    Fraction1.div2(Fraction2);
                    Fraction1.reduce();
                    Console.WriteLine($"Fraction1 = Fraction1/Fraction2 = {Fraction1.toString()}");
                    Console.WriteLine($"Fraction1 Value = {Fraction1.value()}");
                    Console.WriteLine($"Are Fraction1 and Fraction2 same? - {Fraction1.equals(Fraction2)}");
                    Console.WriteLine($"numberPart of Fraction2 = {Fraction2.numberPart()}");
                    break;
            }
        }
    }
}
