﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Input(out string name, out double price, out int count, out int sale)
        {
            Console.WriteLine("Enter name");
            name = Console.ReadLine();
            Console.WriteLine("Enter price");
            price = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter count");
            count = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter sale");
            sale = Convert.ToInt32(Console.ReadLine());
        }
        static double SrAr(double a,double b)
        {
            return ((a + b) / 2);
        }
        static double SrAr(double a, double b,double c)
        {
            return ((a + b+c) / 3);
        }
        static double SrAr(double a, double b,double c , double d)
        {
            return ((a + b+c+d) / 4);
        }
        static int HappyT(int a)
        {
            if (a/100000 + a%100000/10000 + a%10000/1000 == a%1000/100 + a%100/10 + a % 10)
            {
                return 1;
            } else { return 0; }
        }
        static int HappyT(int a,int b)
        {
            if (a / 100 + a % 100 / 10 + a % 10 == b / 100 + b % 100 / 10 + b % 10)
            {
                return 1;
            }
            else { return 0; }
        }
        static int HappyT(int a, int b,int c,int d,int e,int f)
        {
            if (a + b + c == d + e + f)
            {
                return 1;
            }
            else { return 0; }
        }
        static void Output(ref string name, ref double price, ref int count, ref int sale)
        {
            Console.WriteLine($"name = {name}");
            Console.WriteLine($"price = {price}");
            Console.WriteLine($"count = {count}");
            Console.WriteLine($"sale = {sale}");
        }
        static double PriceAll(double price, int count, int sale)
        {
            return count * (price - sale);
        }
        static double Min(double a, double b, double c)
        {
            if (a < b && a < c)
            {
                return a;
            }
            else if (b < a && b < c)
            {
                return b;
            }
            else
            {
                return c;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Task 1");
            string name;
            double price;
            int count;
            int sale;
            double priceall = 0;
            Console.WriteLine("Product1");
            Input(out name, out price, out count, out sale);
            Console.WriteLine("Product 1");
            Output(ref name, ref price, ref count, ref sale);
            Console.WriteLine($"Price with count and sale = {PriceAll(price, count, sale)}");
            double priceall1 = PriceAll(price, count, sale);
            priceall += PriceAll(price, count, sale);
            Console.WriteLine("Product2");
            Input(out name, out price, out count, out sale);
            Console.WriteLine("Product 2");
            Output(ref name, ref price, ref count, ref sale);
            Console.WriteLine($"Price with count and sale = {PriceAll(price, count, sale)}");
            double priceall2 = PriceAll(price, count, sale);
            priceall += PriceAll(price, count, sale);
            name = "Boeing 777";
            price = 2;
            count = 1000000;
            sale = 0;
            Console.WriteLine("Product 3");
            Output(ref name, ref price, ref count, ref sale);
            Console.WriteLine($"Price with count and sale = {PriceAll(price, count, sale)}");
            double priceall3 = PriceAll(price, count, sale);
            priceall += PriceAll(price, count, sale);
            Console.WriteLine($"Price of all products with count and sale = {priceall}");
            Console.WriteLine($"Min = {Min(priceall1, priceall2, priceall3)}");
            Console.WriteLine("Task 2");
            Console.WriteLine("Enter a");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter b");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"SredneeArefm for 2 numbers = {SrAr(a, b)}");
            Console.WriteLine("Enter c");
            double c = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"SredneeArefm for 3 numbers = {SrAr(a, b,c)}");
            Console.WriteLine("Enter d");
            double d = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"SredneeArefm for 4 numbers = {SrAr(a, b,c,d)}");
            Console.WriteLine("Enter SredneeArefm numbers count you need to do");
            int n = Convert.ToInt32(Console.ReadLine());
            if (n == 2)
            {
                Console.WriteLine($"SredneeArefm for 2 numbers = {SrAr(a, b)}");
            }
            if (n == 3)
            {
                Console.WriteLine($"SredneeArefm for 3 numbers = {SrAr(a, b, c)}");
            }
            if (n == 4)
            {
                Console.WriteLine($"SredneeArefm for 4 numbers = {SrAr(a, b, c, d)}");
            }
            Console.WriteLine("Task 3");
            int result = 0;
            Console.WriteLine("Enter Ticket's numbers");
            string Ticket1 = Console.ReadLine();
            if (Ticket1.Length == 6)
            {
                result = HappyT(Convert.ToInt32(Ticket1));
            } else if (Ticket1.Length == 3)
            {
                string Ticket2 = Console.ReadLine();
                result = HappyT(Convert.ToInt32(Ticket1), Convert.ToInt32(Ticket2));
            } else if (Ticket1.Length == 1)
            {
                string Ticket2 = Console.ReadLine();
                string Ticket3 = Console.ReadLine();
                string Ticket4 = Console.ReadLine();
                string Ticket5 = Console.ReadLine();
                string Ticket6 = Console.ReadLine();
                result = HappyT(Convert.ToInt32(Ticket1), Convert.ToInt32(Ticket2),Convert.ToInt32(Ticket3), Convert.ToInt32(Ticket4),Convert.ToInt32(Ticket5), Convert.ToInt32(Ticket6));
            }
            if (result == 1)
            {
                Console.WriteLine("This is the happy ticket");
            } else {
                Console.WriteLine("This is not the happy ticket");
                    }
        }
    }
}
