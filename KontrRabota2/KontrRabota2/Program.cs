﻿using System;

namespace KontrRabota2
{
    class Player
    {
        public string name { get; set; }
        public int hp { get; set; }
        public Player()
        {
            Console.WriteLine("Enter Name of Player");
            name = Console.ReadLine();
            Console.WriteLine("Enter HP of Player");
            hp = Convert.ToInt32(Console.ReadLine());
        }
    }
    class Game
    {
        public void Attack(Player player1, Player player2)
        {
            Console.WriteLine($"{player1.name} attack {player2.name}");
            Console.WriteLine("Enter attackpower");
            attackpower = Convert.ToInt32(Console.ReadLine());
            player2.hp -= attackpower;
        }
        int attackpower { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Variant 4
        Player player1 = new Player();
        Player player2 = new Player();
            Game game = new Game();
            
            do
                {
                    game.Attack(player1, player2);
                    game.Attack(player2, player1);
                } while (player1.hp > 0 || player2.hp > 0);
            if (player1.hp > player2.hp) Console.WriteLine($"{player1.name} win");
            else Console.WriteLine($"{player2.name} win");
        }
    }
}
