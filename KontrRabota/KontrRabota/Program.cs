﻿using System;
using System.IO;

namespace KontrRabota
{
    class Price
    {
        public string name { get; set; }
        public string shopname { get; set; }
        public double price { get; set; }
        public Price()
        {
            Console.WriteLine("Enter name");
            name = Console.ReadLine();
            Console.WriteLine("Enter name of shop");
            shopname = Console.ReadLine();
            Console.WriteLine("Enter price");
            price = Convert.ToDouble(Console.ReadLine());
        }
        public Price(string name,string shopname,double price)
        {
            this.name = name;
            this.shopname = shopname;
            this.price = price;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Variant 4 Ishbuldin Ilgiz
            Price[] goodsin = new Price[5];
            var input = new StreamWriter(@"c:\Text\input.txt");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"good {i + 1}");
                goodsin[i] = new Price();
                input.WriteLine(goodsin[i].name);
                input.WriteLine(goodsin[i].shopname);
                input.WriteLine(goodsin[i].price);
            }
            var output = new StreamWriter(@"c:\Text\output.txt");
            Price[] goodsout = new Price[5];
            for (int i = 0; i < 5; i++)
            {
                goodsout[i] = new Price(goodsin[i].name, goodsin[i].shopname, goodsin[i].price);
            }
            input.Close();
            Price vr = new Price("T", "t", 2);
            for (int i = 0; i < goodsout.Length - 1; i++)
            {
                for (int j = 0; j < goodsout.Length - i - 1; j++)
                {
                    if (goodsout[j + 1].price > goodsout[j].price)
                    {
                        vr = goodsout[j + 1];
                        goodsout[j + 1] = goodsout[j];
                        goodsout[j] = vr;
                    }
                }
            }
            for (int i = 0; i < 5; i++)
            {
                output.WriteLine(goodsout[i].name);
                output.WriteLine(goodsout[i].shopname);
                output.WriteLine(goodsout[i].price);
            }
            Console.WriteLine("Enter name of good");
            string textname = Console.ReadLine();
            bool have = false;
            int count = 1;
            for (int i = 0; i < 5; i++)
            {
                if(goodsout[i].name == textname)
                {
                    output.WriteLine($"Serchin good {count}:");
                    output.WriteLine(goodsout[i].name);
                    output.WriteLine(goodsout[i].shopname);
                    output.WriteLine(goodsout[i].price);
                    have = true;
                    count++;
                }
            }
            if(!have) output.WriteLine($"Don't have goods with this name:{textname}");
            output.Close();
        }
    }
}
