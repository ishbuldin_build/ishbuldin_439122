﻿using System;

namespace LabRabota14
{
    abstract class Song
    {
        public string SongName { get; set; }
        public int Rate { get; set; }
        public Song(string name, int rate)
        {
            SongName = name;
            Rate = rate;
        }
        public Song()
        {
            Console.WriteLine("Write song (Author - SongName)");
            SongName = Console.ReadLine();
            Console.WriteLine("Write Rate of this song(0 - 10)");
            Rate = Convert.ToInt32(Console.ReadLine());
        }
        public Song(string name)
        {
            SongName = name;
            Console.WriteLine("Write Rate of this song(0 - 10)");
            Rate = Convert.ToInt32(Console.ReadLine());
        }
        public string Writer()
        {
            string writer = "";
            int i = 0;
            do
            {
                writer += SongName[i];
                i++;
            }
            while (SongName[i + 1] != Convert.ToChar("-"));
            return writer;
        }
        public void Print()
        {
            Console.WriteLine($"Author = {SongName}");
            Console.WriteLine($"Rate = {Rate}");
        }
        public abstract string ListenOrNot();
    }
    class Pop : Song
    {
        public int FunRate;
        public Pop() : base()
        {
            Console.WriteLine("How fun this song(0 - 10)");
            FunRate = Convert.ToInt32(Console.ReadLine());
        }
        public Pop(string name, int rate, int funrate)
        {
            SongName = name;
            Rate = rate;
            FunRate = funrate;
        }
        public string HowFun()
        {
            if (FunRate < 2) return "Song is very sad";
            else if (FunRate < 4) return "Song is sad";
            else if (FunRate < 6) return "Song is good";
            else if (FunRate < 9) return "Song is Funny";
            else return "Song is very funny";
        }
        public override string ListenOrNot()
        {
            if ((FunRate + Rate) / 2 > 5) return "Yes";
            else return "No";
        }
        public new void Print()
        {
            Console.WriteLine($"Pop song name = {SongName}");
            Console.WriteLine($"Rate = {Rate}");
            Console.WriteLine($"FunRate = {FunRate}");
        }
    }
    class Rock : Song
    {
        public int HardRate;
        public Rock() : base()
        {
            Console.WriteLine("How hard this song(0 - 10)");
            HardRate = Convert.ToInt32(Console.ReadLine());
        }
        public Rock(string name, int rate, int hardrate)
        {
            SongName = name;
            Rate = rate;
            HardRate = hardrate;
        }
        public string HowHard()
        {
            if (HardRate < 2) return "Rock song is very easy";
            else if (HardRate < 4) return "Rock song s easy";
            else if (HardRate < 6) return "Rock song is normal";
            else if (HardRate < 9) return "Rock song is hard";
            else return "Rock song is very hard";
        }
        public override string ListenOrNot()
        {
            if ((HardRate + Rate) / 2 > 5) return "Yes";
            else return "No";
        }
        public new void Print()
        {
            Console.WriteLine($"Rock song name = {SongName}");
            Console.WriteLine($"Rate = {Rate}");
            Console.WriteLine($"HardRate = {HardRate}");
        }
    }
    class Rap : Song
    {
        public int FlowRate;
        public Rap() : base()
        {
            Console.WriteLine("How is flow of this song(0 - 10)");
            FlowRate = Convert.ToInt32(Console.ReadLine());
        }
        public Rap(string name, int rate, int flowrate)
        {
            SongName = name;
            Rate = rate;
            FlowRate = flowrate;
        }
        public string HowFlow()
        {
            if (FlowRate < 2) return "Flow of this rap song is very slow";
            else if (FlowRate < 4) return "Flow of this rap song is slow";
            else if (FlowRate < 6) return "Flow of this rap song is normal";
            else if (FlowRate < 9) return "Flow of this rap song is fast";
            else return "Flow of this rap song is very fast";
        }
        public override string ListenOrNot()
        {
            if ((FlowRate + Rate) / 2 > 5) return "Yes";
            else return "No";
        }
        public new void Print()
        {
            Console.WriteLine($"Rap song name = {SongName}");
            Console.WriteLine($"Rate = {Rate}");
            Console.WriteLine($"FlowRate = {FlowRate}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            int choose;
            Console.WriteLine("Enter counts of songs");
            int n = Convert.ToInt32(Console.ReadLine());
            Song[] songs = new Song[n];
            int[] numbers = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Switch genre of song,if song is Pop - 1,if Rock - 2,if Rap - 3");
                choose = Convert.ToInt32(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        Pop pop = new Pop();
                        songs[i] = pop;
                        numbers[i] = 1;
                        break;
                    case 2:
                        Rock rock = new Rock();
                        songs[i] = rock;
                        numbers[i] = 2;
                        break;
                    case 3:
                        Rap rap = new Rap();
                        songs[i] = rap;
                        numbers[i] = 3;
                        break;
                    default:
                        Console.WriteLine("Wrong genre of song(1,2 or 3)");
                        break;
                }
            }
            Console.WriteLine("Print:");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Writer - {songs[i].Writer()}");
                songs[i].Print();
                Console.WriteLine($"Need to Listen: {songs[i].ListenOrNot()}");
            }
        }
    }
}
