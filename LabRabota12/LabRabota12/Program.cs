﻿using System;
using System.ComponentModel;

namespace LabRabota12
{
    public class RationalFraction
    {
        public int Up { get; set; }
        public int Down { get; set; }
        public RationalFraction()
        {
            Up = 0;
            Down = 1;
        }
        public RationalFraction(int up,int down)
        {
            this.Up = up;
            this.Down = down;
        }
        public string toString()
        {
            return $"{Up}/{Down}";
        }
        public void reduce()
        {
            int a = this.Up;
            int b = this.Down;
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }
            this.Up = this.Up / a;
            this.Down = this.Down / a;
            if (this.Up >= 0 && this.Down < 0)
            {
                this.Up = -this.Up;
                this.Down *= (-1);
            }
            if (this.Up < 0 && this.Down < 0)
            {
                this.Up *= (-1);
                this.Down *= (-1);
            }
        }
    }
    public class RationalVector2D
    {
        public RationalFraction x;
        public RationalFraction y;

        public RationalVector2D()
        {
            x = new RationalFraction();
            y = new RationalFraction();
        }
        public RationalVector2D(RationalFraction a, RationalFraction b)
        {
            x = a;
            y = b;
        }
        public RationalVector2D add(RationalVector2D a)
        {
            RationalVector2D vector2D = new RationalVector2D();
            vector2D.x.Up = this.x.Up * a.x.Down + this.x.Down * a.x.Up;
            vector2D.x.Down = this.x.Down * a.x.Down;
            vector2D.y.Up = this.y.Up * a.y.Down + this.y.Down * a.y.Up;
            vector2D.y.Down = this.y.Down * a.y.Down;
            vector2D.x.reduce();
            vector2D.y.reduce();
            return vector2D;
        }
        public double length()
        {
            return Math.Pow((Math.Pow(x.Up,2)/ Math.Pow(x.Down, 2))+(Math.Pow(y.Up, 2)/ Math.Pow(y.Down, 2)), 0.5);
        }
        public RationalFraction scalarProduct(RationalVector2D a)
        {
            RationalFraction Rfraction = new RationalFraction();
            Rfraction.Up = this.x.Up *a.x.Up* this.y.Down * a.y.Down+ this.x.Down * a.x.Down *this.y.Up*a.y.Up;
            Rfraction.Down =this.x.Down*a.x.Down*this.y.Down*a.y.Down;
            Rfraction.reduce();
            return Rfraction;
        }
        public bool equals(RationalVector2D a)
        {
            this.x.reduce();
            this.y.reduce();
            a.x.reduce();
            a.y.reduce();
            if (this.x.Up.Equals(a.x.Up)&& this.x.Down.Equals(a.x.Down)&& this.y.Up.Equals(a.y.Up)&& this.y.Down.Equals(a.y.Down)) return true;
            else return false;
        }
    }
    class Person
    {
        public static int count = 0;
        public string surname { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public char pol { get; set; }
        public double age;
        public Person(string surname, string name, DateTime date,char pol)
        {
            this.surname = surname;
            this.name = name;
            this.date = date;
            this.pol = pol;
            this.age = Math.Truncate((DateTime.Now - this.date).TotalDays / 365.25);
            count++;
        }
        public Person(string surname,string name,DateTime date)
        {
            this.surname = surname;
            this.name = name;
            this.date = date;
            Console.WriteLine("Write f - female or m - male");
            this.pol = Convert.ToChar(Console.ReadLine());
            this.age = Math.Truncate((DateTime.Now - this.date).TotalDays / 365.25);
            count++;
        }
        public Person()
        {
            Console.WriteLine("Write surname");
            this.surname = Console.ReadLine();
            Console.WriteLine("Write name");
            this.name = Console.ReadLine();
            Console.WriteLine("Write Date");
            this.date = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Write f - female or m - male");
            this.pol = Convert.ToChar(Console.ReadLine());
            this.age = Math.Truncate((DateTime.Now - this.date).TotalDays / 365.25);
            count++;
        }
        public double Age()
        {
            return Math.Truncate((DateTime.Now - this.date).TotalDays/365.25);
        }
        public void Passport()
        {
            Console.WriteLine($"{surname} {(name)[0]},{age},{pol}") ;
        }
        public static Person[] persons = new Person[4];
    }
    class Worker : Person
    {
        public static double nalogsum = 0;
        private static int nalog = 13;
        public double oklad { get; set; }
        public int bonusper { get; set; }
        public int experience { get; set; }
        public Worker(string surname, string name, DateTime date, char pol,double oklad, int bonusper,int experience):base(surname,name,date,pol)
        {
            this.oklad = oklad;
            this.bonusper = bonusper;
            this.experience = experience;
        }
        public Worker(int oklad) : base()
        {
            this.oklad = oklad;
            Console.WriteLine("Write bonus in percent(%)");
            bonusper = Convert.ToInt32(Console.ReadLine());
            if (bonusper>100 || bonusper<0)
            {
                Console.WriteLine("Wrong answer.Write again (0% <= BonusPercent <= 100%)");
                bonusper = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Write experience in years");
            experience = Convert.ToInt32(Console.ReadLine());
        }
        public Worker() : base()
        {
            Console.WriteLine("Write oklad");
            oklad = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write bonus in percent(%)");
            bonusper = Convert.ToInt32(Console.ReadLine());
            if (bonusper > 100 || bonusper < 0)
            {
                Console.WriteLine("Wrong answer.Write again (0% <= BonusPercent <= 100%)");
                bonusper = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Write experience in years");
            experience = Convert.ToInt32(Console.ReadLine());
        }
        public void Print()
        {
            Console.WriteLine($"Surname - {surname}");
            Console.WriteLine($"Name - {name}");
            Console.WriteLine($"Age - {age}");
            Console.WriteLine($"Pol - {pol}");
            Console.WriteLine($"Oklad - {oklad}");
            Console.WriteLine($"Bonus - {bonusper}%");
            Console.WriteLine($"Experience in years - {experience}");
        }
        public double Salary()
        {
            return oklad + (oklad / 100 * bonusper);
        }
        public double Nalog()
        {
            return (oklad + (oklad / 100 * bonusper)) / 100 * nalog;
        }
        public double HandSalary()
        {
            return oklad + (oklad / 100 * bonusper) - ((oklad + (oklad / 100 * bonusper)) / 100 * nalog);
        }
        public virtual void UpSalary()
        {
            if (experience > 10) bonusper *= 2;
        }
        public static Worker[] workers = new Worker[5];
    }
    class HourWorkers : Worker
    {
        private double salaryhour { get; set; }
        private int hoursformonth { get; set; }
        public HourWorkers():base(0)
        {
            Console.WriteLine("Write your salary/hour");
            salaryhour = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Write your hours/month");
            hoursformonth = Convert.ToInt32(Console.ReadLine());
        }
        public HourWorkers(string surname, string name, DateTime date, char pol, double oklad, int bonusper, int experience, double salaryhour,int hoursformonth) : base(surname, name, date, pol, oklad, bonusper, experience)
        {
            this.oklad = 0;
            this.salaryhour = salaryhour;
            this.hoursformonth = hoursformonth;
        }
        public void PrintHW()
        {
            Console.WriteLine($"Surname - {surname}");
            Console.WriteLine($"Name - {name}");
            Console.WriteLine($"Age - {age}");
            Console.WriteLine($"Pol - {pol}");
            Console.WriteLine($"Bonus - {bonusper}%");
            Console.WriteLine($"Experience in years - {experience}");
            Console.WriteLine($"Salary - {salaryhour*hoursformonth}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of Task: 1 or 2");
            int task = Convert.ToInt32(Console.ReadLine());
            switch (task)
            {
                case 1:
                    RationalVector2D RationalVectorNull = new RationalVector2D();
                    Console.WriteLine($"RationalVectorNull = ({RationalVectorNull.x.toString()}:{RationalVectorNull.y.toString()})");
                    Console.WriteLine("Enter x, like Fraction: Numerator then Denominator");
                    int xup = Convert.ToInt32(Console.ReadLine());
                    int xdown = Convert.ToInt32(Console.ReadLine());
                    RationalFraction RF1 = new RationalFraction(xup,xdown);
                    Console.WriteLine("Enter y, like Fraction: Numerator then Denominator");
                    int yup = Convert.ToInt32(Console.ReadLine());
                    int ydown = Convert.ToInt32(Console.ReadLine());
                    RationalFraction RF2 = new RationalFraction(yup, ydown);
                    RationalVector2D RationalVector1 = new RationalVector2D(RF1, RF2);
                    Console.WriteLine($"RationalVector1 = ({RationalVector1.x.toString()}:{RationalVector1.y.toString()})");
                    Console.WriteLine("Enter x, like Fraction: Numerator then Denominator");
                    xup = Convert.ToInt32(Console.ReadLine());
                    xdown = Convert.ToInt32(Console.ReadLine());
                    RF1 = new RationalFraction(xup, xdown);
                    Console.WriteLine("Enter y, like Fraction: Numerator then Denominator");
                    yup = Convert.ToInt32(Console.ReadLine());
                    ydown = Convert.ToInt32(Console.ReadLine());
                    RF2 = new RationalFraction(yup, ydown);
                    RationalVector2D RationalVector2 = new RationalVector2D(RF1, RF2);
                    Console.WriteLine($"RationalVector2 = ({RationalVector2.x.toString()}:{RationalVector2.y.toString()})");
                    RationalVector2D RationalVectorSumm = RationalVector1.add(RationalVector2);
                    Console.WriteLine($"RationalVectorSumm = ({RationalVectorSumm.x.toString()}:{RationalVectorSumm.y.toString()})");
                    Console.WriteLine($"length of RationalVector1 = {RationalVector1.length()}");
                    RationalFraction scalarProduct = RationalVector1.scalarProduct(RationalVector2);
                    Console.WriteLine($"scalarProduct = {scalarProduct.toString()}");
                    Console.WriteLine($"RationalVector1 = RationalVector2 - {RationalVector1.equals(RationalVector2)}");
                    break;
                case 2:
                    Console.WriteLine("1) Persons");
                    Console.WriteLine("First - Ivanov,Nikita,24.11.2001,m");
                    DateTime datefirst = new DateTime(2001 ,11, 24);
                    Person.persons[0] = new Person("Ivanov","Nikita",datefirst);
                    Console.WriteLine($"Age = {Person.persons[0].Age()}");
                    DateTime datesecond = new DateTime(1998, 1, 4);
                    Person.persons[1] = new Person("Petrov", "Alexey", datesecond,Convert.ToChar("m"));
                    DateTime datethird = new DateTime(2003, 12, 1);
                    Person.persons[2] = new Person("Ionova", "Yana", datethird, Convert.ToChar("f"));
                    Person.persons[3] = new Person();
                    foreach (Person x in Person.persons) x.Passport();
                    Console.WriteLine("2A) Workers");
                    DateTime datew1 = new DateTime(1975, 11, 1);
                    Worker workerone = new Worker("Petrov", "Ivan", datew1, Convert.ToChar("m"), 40000, 15, 2);
                    Worker workertwo = new Worker(30000);
                    Console.WriteLine("WorkerOne information:");
                    workerone.Print();
                    Console.WriteLine($"Salary = {workerone.Salary()}");
                    Console.WriteLine($"HandSalary = {workerone.HandSalary()}");
                    Console.WriteLine("WorkerTwo information:");
                    workertwo.Print();
                    Console.WriteLine($"Salary = {workertwo.Salary()}");
                    Console.WriteLine($"HandSalary = {workertwo.HandSalary()}");
                    Console.WriteLine("2B) Workers");
                    Console.WriteLine("Worker 1");
                    Worker.workers[0] = new Worker(40000);
                    Worker.workers[0].UpSalary();
                    Worker.nalogsum += Worker.workers[0].Nalog();
                    Console.WriteLine("Worker 2");
                    Worker.workers[1] = new Worker(50000);
                    Worker.workers[1].UpSalary();
                    Worker.nalogsum += Worker.workers[1].Nalog();
                    Console.WriteLine("Worker 3");
                    Worker.workers[2] = new Worker(30000);
                    Worker.workers[2].UpSalary();
                    Worker.nalogsum += Worker.workers[2].Nalog();
                    Console.WriteLine("Worker 4");
                    Worker.workers[3] = new Worker(20000);
                    Worker.workers[3].UpSalary();
                    Worker.nalogsum += Worker.workers[3].Nalog();
                    Console.WriteLine("Worker 5");
                    Worker.workers[4] = new Worker(40000);
                    Worker.workers[4].UpSalary();
                    Worker.nalogsum += Worker.workers[4].Nalog();
                    int maxindex=0;
                    for (int i = 1; i < Worker.workers.Length; i++)
                    {
                        if (Worker.workers[i].HandSalary() > Worker.workers[maxindex].HandSalary()) maxindex = i;
                    }
                    Console.WriteLine($"Summ of Nalog - {Worker.nalogsum}");
                    Console.WriteLine($"Maximum HandSalary - {Worker.workers[maxindex].surname}");
                    Console.WriteLine("3) HourWorkers");
                    HourWorkers FirstWorker = new HourWorkers();
                    FirstWorker.UpSalary();
                    DateTime datahw = new DateTime(1977,10,11);
                    HourWorkers SecondWorker = new HourWorkers("Petrov","Ilya",datahw,Convert.ToChar("m"),0,20,20,300,25);
                    SecondWorker.UpSalary();
                    Console.WriteLine("First Worker:");
                    FirstWorker.PrintHW();
                    Console.WriteLine("Second Worker:");
                    SecondWorker.PrintHW();
                    break;
            }
        }
    }
}
