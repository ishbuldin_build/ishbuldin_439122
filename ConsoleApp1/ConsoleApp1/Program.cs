﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1)Enter Perimeter");
            double p = Convert.ToDouble(Console.ReadLine());
            double s = 0.5 * Math.Pow(p / 3, 2) * Math.Sin(Math.PI / 3);
            Console.WriteLine($"Area ={s:f2}");
            Console.WriteLine("2)Enter two numbers");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            s = (Math.Pow(a, 3) + Math.Pow(b, 3)) / 2;
            Console.WriteLine($"Arithmetic mean of two cubes ={s:f2}");
            Console.WriteLine("3)Enter the legs");
            a = Convert.ToDouble(Console.ReadLine());
            b = Convert.ToDouble(Console.ReadLine());
            double g = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            Console.WriteLine($"Hypotenuse ={g:f2}");
            Console.WriteLine("4)Enter the legs");
            a = Convert.ToDouble(Console.ReadLine());
            b = Convert.ToDouble(Console.ReadLine());
            g = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            p = a + b + g;
            Console.WriteLine($"P = {p:f2}");
            Console.WriteLine("5)Enter the volume of the cube");
            double v = Convert.ToDouble(Console.ReadLine());
            double e = Math.Pow(v, 1.0 / 3.0);
            Console.WriteLine($"Cube edge ={e:f2}");
            Console.WriteLine("6)Enter the coordinates: x1,y1,x2,y2,x3,y3");
            double x1 = Convert.ToDouble(Console.ReadLine());
            double y1 = Convert.ToDouble(Console.ReadLine());
            double x2 = Convert.ToDouble(Console.ReadLine());
            double y2 = Convert.ToDouble(Console.ReadLine());
            double x3 = Convert.ToDouble(Console.ReadLine());
            double y3 = Convert.ToDouble(Console.ReadLine());
            double s1 = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
            double s2 = Math.Sqrt(Math.Pow(x2 - x3, 2) + Math.Pow(y2 - y3, 2));
            double s3 = Math.Sqrt(Math.Pow(x1 - x3, 2) + Math.Pow(y1 - y3, 2));
            p = s1 + s2 + s3;
            Console.WriteLine($"P = {p:f2}");
            Console.WriteLine("7)Enter the circumference");
            double c = Convert.ToDouble(Console.ReadLine());
            double r = c / (2 * Math.PI);
            Console.WriteLine($"Radius of circle ={r:f2}");
            Console.WriteLine("8)Enter the base(a and b, b>a) of the trapezoid and angle(Degress) near long base");
            a = Convert.ToDouble(Console.ReadLine());
            b = Convert.ToDouble(Console.ReadLine());
            double angle = Convert.ToDouble(Console.ReadLine());
            double h = Math.Tan(Math.PI / 180 * angle) * (b - a) / 2;
            s = (a + b) / 2 * h;
            Console.WriteLine($"Radius of circle ={s:f2}");
            Console.WriteLine("9)Enter the side of the regular triangle");
            a = Convert.ToDouble(Console.ReadLine());
            r = a / (2 * Math.Sqrt(3));
            Console.WriteLine($"Radius of circle ={r:f2}");
            Console.WriteLine("10)Enter a1,d,n");
            double a1 = Convert.ToDouble(Console.ReadLine());
            double d = Convert.ToDouble(Console.ReadLine());
            double n = Convert.ToDouble(Console.ReadLine());
            s = (n / 2) * (2 * a1 + (n - 1) * d);
            Console.WriteLine($"Amount ={s:f2}");
        }
    }
}
