﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter x and y");
            double x = Convert.ToDouble(Console.ReadLine());
            double y = Convert.ToDouble(Console.ReadLine());
            if (x * x + y * y <= 81 && x >= 0)
            {
                Console.WriteLine("1)YES");
            }
            else Console.WriteLine("1)NO");
            if (x * x + y * y <= 100 && x * x + y * y >= 25 && y > 0)
            {
                Console.WriteLine("2)YES");
            }
            else Console.WriteLine("2)NO");
            if (x * x + y * y <= 49 && x * x + y * y >= 9)
            {
                Console.WriteLine("5)YES");
            }
            else Console.WriteLine("5)NO");
            if (x * x + y * y >= 625 && x * x + y * y <= 215)
            {
                Console.WriteLine("6)YES");
            }
            else Console.WriteLine("6)NO");
            if (x * x + y * y <= 215 && y >= Math.Abs(x))
            {
                Console.WriteLine("7)YES");
            }
            else Console.WriteLine("7)NO");
            Console.WriteLine("Enter a month number");
            byte m = Convert.ToByte(Console.ReadLine());
            switch (m)
            {
                case 1:Console.WriteLine("11"); break;
                case 2: Console.WriteLine("10"); break;
                case 3: Console.WriteLine("9"); break;
                case 4: Console.WriteLine("8"); break;
                case 5: Console.WriteLine("7"); break;
                case 6: Console.WriteLine("6"); break;
                case 7: Console.WriteLine("5"); break;
                case 8: Console.WriteLine("4"); break;
                case 9: Console.WriteLine("3"); break;
                case 10: Console.WriteLine("2"); break;
                case 11: Console.WriteLine("1"); break;
                case 12: Console.WriteLine("0"); break;
                default:Console.WriteLine("Error");break;
            }
            Console.WriteLine("Enter Mast' number");
            byte n = Convert.ToByte(Console.ReadLine());
            switch (n)
            {
                case 1: Console.WriteLine("PIKI"); break;
                case 2: Console.WriteLine("Trefi"); break;
                case 3: Console.WriteLine("Bubni"); break;
                case 4: Console.WriteLine("4ervi"); break;
                default: Console.WriteLine("ERROR: This is not right number"); break;
            }
            Console.WriteLine("Enter Mast' and NomerDostoinstva");
            m = Convert.ToByte(Console.ReadLine());
            byte k = Convert.ToByte(Console.ReadLine());
            switch (k)
            {
                case 6: Console.Write("Six "); break;
                case 7: Console.Write("Seven "); break;
                case 8: Console.Write("Eight "); break;
                case 9: Console.Write("Nine "); break;
                case 10: Console.Write("Ten "); break;
                case 11: Console.Write("Valet "); break;
                case 12: Console.Write("Queen "); break;
                case 13: Console.Write("King "); break;
                case 14: Console.Write("Tuz "); break;
                default: Console.WriteLine("ERROR: This is not right number"); break;
            }
            switch (m)
            {
                case 1: Console.Write("PIKI"); break;
                case 2: Console.Write("Trefi"); break;
                case 3: Console.Write("Bubni"); break;
                case 4: Console.Write("4ervi"); break;
                default: Console.WriteLine("ERROR: This is not right number"); break;
            }
            Console.WriteLine();
            Console.WriteLine("Enter the number day of week(1,2,3,4,5,6 or 7)");
            int d = Convert.ToInt32(Console.ReadLine());
            switch (d)
            {
                case 1: Console.WriteLine("On Monday: 9 - 21"); break;
                case 2: Console.WriteLine("On Tuesday: 9 - 21"); break;
                case 3: Console.WriteLine("On Wednesday: 9 - 21"); break;
                case 4: Console.WriteLine("On Thursday: 10 - 20"); break;
                case 5: Console.WriteLine("On Friday: 10 - 20"); break;
                case 6: Console.WriteLine("On Saturday: 12-17"); break;
                case 7: Console.WriteLine("On Sunday: No work"); break;
                default: Console.WriteLine("ERROR: This is not right number"); break;
            }
            Console.WriteLine("Enter your points");
            int p = Convert.ToInt32(Console.ReadLine());
            if (p < 50)
                Console.WriteLine("BAD");
            else if (p < 70)
                Console.WriteLine("NOT BAD");
            else if (p < 90)
                Console.WriteLine("GOOD");
            else if (p <= 100)
                Console.WriteLine("VERY GOOD");
            else Console.WriteLine("Error");
            Console.WriteLine("Enter type of Figure");
            string f = Console.ReadLine();
            if (f == "k")
            {
                d = 1;
            }
            if (f == "p")
            {
                d = 2;
            }
            if (f == "t")
            {
                d = 3;
            }
            switch (d)
            {
                case 1: Console.WriteLine("Enter radius(KRUG)");
                    double r = Convert.ToDouble(Console.ReadLine());
                    double per = 2 * Math.PI * r;
                    double plo = Math.PI * Math.Pow(r, 2);
                    Console.WriteLine($"Perimetr = {per:f2}");
                    Console.WriteLine($"Ploshad = {plo:f2}");
                    break;
                case 2:
                    Console.WriteLine("Enter sides(PRYAMOUGOLNIK)");
                    double a = Convert.ToDouble(Console.ReadLine());
                    double b = Convert.ToDouble(Console.ReadLine());
                    per = 2 * a+2*b;
                    plo = a*b;
                    Console.WriteLine($"Perimetr = {per:f2}");
                    Console.WriteLine($"Ploshad = {plo:f2}");
                    break;
                case 3:
                    Console.WriteLine("Enter sides(TREUGOLNIK)");
                    a = Convert.ToDouble(Console.ReadLine());
                    b = Convert.ToDouble(Console.ReadLine());
                    double c = Convert.ToDouble(Console.ReadLine());
                    per = a+b+c;
                    double pper = per / 2;
                    plo = Math.Pow(pper *(pper - a)*(pper - b) * (pper - c ),0.5);
                    Console.WriteLine($"Perimetr = {per:f2}");
                    Console.WriteLine($"Ploshad = {plo:f2}");
                    break;
                default: Console.WriteLine("ERROR:"); break;
            }
        }
    }
}
