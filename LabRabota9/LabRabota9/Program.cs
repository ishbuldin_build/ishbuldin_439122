﻿using System;
using System.Text.RegularExpressions;


namespace LabRabota9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of task");
            int task = Convert.ToInt32(Console.ReadLine());
            switch(task)
            {
                case 1:
                    Console.WriteLine("Write anything");
                    string str = Console.ReadLine();
                    Console.WriteLine("Write word");
                    string word = Console.ReadLine();
                    if (Regex.IsMatch(str,@"\b"+$"{word}"+@"\b"))
                    {
                        Console.WriteLine("Yes");
                    }
                    else { Console.WriteLine("No"); }
                    break;
                case 2:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    Console.WriteLine("Write length of word");
                    int wlength = Convert.ToInt32(Console.ReadLine());
                    foreach(Match match in Regex.Matches(str, @"\b\w+\b"))
                    {
                        if(match.Length == wlength)Console.WriteLine(match);
                    }
                     break;
                case 3:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    foreach (Match match in Regex.Matches(str, @"\b[A-ZА-Я]+\w*\b"))
                    {
                        Console.WriteLine(match);
                    }
                    break;
                case 4:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    foreach (Match match in Regex.Matches(str, @"\b\w+\b"))
                    {
                        Console.Write(match);
                    }
                    break;
                case 5:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    string pattern = @"\b[A-Za-z]+\b";
                    Console.WriteLine(Regex.Replace(str, pattern, "..."));
                    break;
                case 6:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    double summa = 0;
                    foreach (Match match in Regex.Matches(str, @"[-]?\b\d+\.?\d*\b"))
                    {
                        summa += Convert.ToDouble(Convert.ToString(match));
                    }
                    Console.WriteLine($"Summa = {summa}");
                    break;
                case 7:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    pattern = @"\b(\d+)\.(\d+)\.(\d+)\b";
                    Regex data = new Regex(pattern);
                    Match dates = data.Match(str);
                    while (dates.Success)
                    {
                        if ( (int.Parse(dates.Groups[1].ToString()) <= 31) && (int.Parse(dates.Groups[1].ToString()) >= 1))
                        {
                            if ((int.Parse(dates.Groups[2].ToString()) <= 12) && (int.Parse(dates.Groups[2].ToString()) >= 1))
                            {
                                if ((int.Parse(dates.Groups[3].ToString()) <= 2010) && (int.Parse(dates.Groups[3].ToString()) >= 1900))
                                {
                                    Console.WriteLine(dates);
                                }
                            }
                        }
                        dates = dates.NextMatch();
                    }

                    break;
                case 8:
                    Console.WriteLine("Write anything");
                    str = Console.ReadLine();
                    pattern = @"\b(\d+)\.(\d+)\.(\d+)\b";
                    data = new Regex(pattern);
                    dates = data.Match(str);
                    while (dates.Success)
                    {
                        if ((int.Parse(dates.Groups[1].ToString()) <= 31) && (int.Parse(dates.Groups[1].ToString()) >= 1))
                        {
                            if ((int.Parse(dates.Groups[2].ToString()) <= 12) && (int.Parse(dates.Groups[2].ToString()) >= 1))
                            {
                                if ((int.Parse(dates.Groups[3].ToString()) <= 2010) && (int.Parse(dates.Groups[3].ToString()) >= 1900))
                                {
                                    if ((int.Parse(dates.Groups[1].ToString()) == 31) && (int.Parse(dates.Groups[2].ToString()) == 12))
                                    {
                                        Console.WriteLine($"1.1.{ int.Parse(dates.Groups[3].ToString()) + 1}");
                                    }
                                    else
                                    {
                                        if (int.Parse(dates.Groups[1].ToString()) == 31)
                                        {
                                            Console.WriteLine($"1.{int.Parse(dates.Groups[2].ToString()) + 1}" + $".{int.Parse(dates.Groups[3].ToString())}");
                                        }
                                        else
                                        {
                                            Console.WriteLine($"{int.Parse(dates.Groups[1].ToString()) + 1}.{int.Parse(dates.Groups[2].ToString())}.{int.Parse(dates.Groups[3].ToString())}");
                                        }
                                    }
                                }
                            }
                        }
                        dates = dates.NextMatch();
                    }
                    break;
                default:
                    Console.WriteLine("Task doesn't exist");
                    break;
            }
            
        }
    }
}
